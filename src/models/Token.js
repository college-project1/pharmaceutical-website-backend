const mongoose = require('mongoose');

const TokenSchema = mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  token: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('Token', TokenSchema);
