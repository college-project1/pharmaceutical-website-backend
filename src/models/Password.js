const mongoose = require('mongoose');

const PasswordSchema = mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('Password', PasswordSchema);
