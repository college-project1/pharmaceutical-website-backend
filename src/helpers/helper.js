exports.statusCodeResponse = function (code, conflictMessage) {
  switch (code) {
    case '409':
      return `Conflict: ${conflictMessage}`;
    case '422':
      return 'Unprocessable Entity';
    case '500':
      return 'Internal Server Error';

    default:
      return 'Error';
  }
};
