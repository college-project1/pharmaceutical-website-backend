const express = require('express');
const { v4: uuidv4 } = require('uuid');
const { statusCodeResponse } = require('../helpers/helper');
const router = express.Router();
const User = require('../models/User');
const Password = require('../models/Password');
const Token = require('../models/Token');

let conflictMessage = '';

router.post('/signUp', async (req, res) => {
  try {
    if (
      !req.body.name ||
      !req.body.email ||
      !req.body.address ||
      !req.body.password
    )
      throw Error('422');

    const isUserPresent = await User.find({ email: req.body.email });

    if (isUserPresent.length > 0) {
      conflictMessage = 'User already present';
      throw Error('409');
    }

    const user = new User({
      name: req.body.name,
      email: req.body.email,
      address: req.body.address
    });
    const password = new Password({
      email: req.body.email,
      password: req.body.password
    });
    const generateToken = uuidv4();
    const token = new Token({
      email: req.body.email,
      token: generateToken
    });

    await user.save();
    await password.save();
    await token.save();
    res.json({
      status: '200',
      message: 'OK',
      meta: {
        token: generateToken
      }
    });
  } catch (error) {
    console.log(error);
    const errorMessage = statusCodeResponse(error.message, conflictMessage);
    res.json({
      status: error.message,
      message: errorMessage
    });
  }
});

router.get('/signIn', async (req, res) => {
  try {
    if (!req.body.email || !req.body.password) throw Error('422');

    const isUserPresent = await User.find({ email: req.body.email });
    if (isUserPresent.length === 1) {
      const storedPassword = await Password.find({ email: req.body.email });
      if (storedPassword[0].password !== req.body.password) {
        conflictMessage = 'Password Mismatch';
        throw Error('409');
      }
      const tokenData = await Token.find({ email: req.body.email });
      res.json({
        status: '200',
        message: 'OK',
        meta: {
          token: tokenData[0].token
        }
      });
    } else if (isUserPresent.length === 0) {
      conflictMessage = 'Incorrect Email address provided';
      throw Error('409');
    } else throw Error('500');
  } catch (error) {
    console.log(error);
    const errorMessage = statusCodeResponse(error.message, conflictMessage);
    res.json({
      status: error.message,
      message: errorMessage
    });
  }
});

module.exports = router;
