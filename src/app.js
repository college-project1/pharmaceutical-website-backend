const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const port = process.env.PORT || 4000;
require('dotenv/config');
const app = express();

const signUp = require('./routes/user');

// Middlewares
app.use(bodyParser.json());
app.use(cors());
app.use('/sessions', signUp);

// ROUTES
app.get('/', (req, res) => {
  res.send('Home');
});

// Connecting to database
mongoose.connect(
  process.env.DB_CONNECTION,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  () => {
    console.log('Connected');
  }
);

app.listen(port);
